// JavaScript Obtenir l'élément parent parentNode
let note = document.querySelector('.note');
console.log(note.parentNode);

let menu = document.querySelector('.menu-item');
console.log(menu.parentNode);

//Frères et sœurs JavaScript
let actuel = document.querySelector('.current');
let suivent = actuel.nextElementSibling;

document.getElementById('frere').innerHTML = `Le frere de '<li class="current">Customer Support</li>' est ${suivent}`;
console.log(suivent);

let actuelFrere = document.querySelector(".current");
let suiventFrere = actuelFrere.nextElementSibling;

while (suiventFrere) {
    console.log(suiventFrere);
    suiventFrere = suiventFrere.nextElementSibling;
}

// Obtenez les frères et sœurs précédents

let actuSoeur = document.querySelector(".current");
let precedent = actuSoeur.previousElementSibling;
console.log(precedent);

while (precedent) {
    console.log(precedent);
    precedent = precedent.previousElementSibling;
}

//Obtenir tous les frères et sœurs d'un élément

let obtenirFrere = function (famille){
    //collecte des freres
    let freres = [];
    //si pas de frere return pas frere
    if(!famille.parentNode){
        return freres;
    }

    //premier enfant 
    let frere = famille.parentNode.firstChild;

    // collecte freres
    while (frere) {
        if (frere.nodeType === 1 && frere !== famille) {
            freres.push(frere);
        }
        frere = frere.nextSibling;
    }
    return freres;
};

let freres = obtenirFrere(document.querySelector('.current'));
siblingText = freres.map(famille => famille.innerHTML);
console.log(siblingText);