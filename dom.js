document.getElementById('getElById').style.color = 'red';

// get element by name
let btn = document.getElementById('btnRates')
let output = document.getElementById('output')
btn.addEventListener('click', () =>{
    let rates = document.getElementsByName('rate');
    rates.forEach((rate) =>{
        if(rate.checked){
            output.innerText = `Vous avez selectionnez: ${rate.value}`;
        }
    });
});

// get element by tag name
let btn2 = document.getElementById('btnCount');
btn2.addEventListener('click', ()=>{
    let heading = document.getElementsByTagName('h2');
    alert(`Le nombre de h2 est: ${heading.length}`);
});

// get element by class name
let menu = document.getElementById('menu');
let items = menu.getElementsByClassName('item');

let donnee = [].map.call(items, item => item.textContent);

document.getElementById('tag').innerHTML = `Affichage du getElementsByClassName: ${donnee}`;
console.log(donnee);

let elements = document.getElementsByClassName('secondary');
donnee = [].map.call(elements, secondary => secondary.textContent);

document.getElementById('tag2').innerHTML = `Affichage du getElementsByClassName: ${donnee}`;
console.log(donnee);

